# Los geht's

---

## Grafiken

* Entweder selbst zeichnen
    - <https://inkscape.org/>
    - <https://gimp.org/>
* Oder vom Internet:
    - <https://openclipart.org/>
    - <https://opengameart.org/> (dort gibts auch Sounds)

---

## Grafiken für dieses Spiel

<https://gitlab.com/silwol/programmiereinstieg_mit_snap/tree/master/assets>

![Herunterladen](02_graphics/download_assets.png "Herunterladen")
