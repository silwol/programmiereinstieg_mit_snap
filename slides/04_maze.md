## Ein Labyrinth

![Screenshot](04_maze/screenshot.png "Screenshot")

---

## Ein kleiner Drache namens 'Scales'

![Drache](04_maze/dragon0.png "Drache")

Quelle: <https://opengameart.org/content/dragon-pixel-art>

---

## Objekteigenschaften

### Benennen

![Objekt benennen](04_maze/rename_object.png "Objekt benennen")

![Objekt benennen](04_maze/rename_scales.png "Objekt benennen")

### Nicht greifbar

![Nicht greifbar](04_maze/scales_not_draggable.png "Nicht greifbar")

### Nur links/rechts drehen

![Nur links/rechts drehen](04_maze/scales_face_left_right.png "Nur links/rechts drehen")

---

### Das Drachenkostüm

![Drache](04_maze/dragon0.png "Drache")

![Kostüm](04_maze/costume_empty.png "Kostüm")

![Kostüm](04_maze/costume_dragon0.png "Kostüm")

---

# Erste Bewegungen

---

## Versuch: nach rechts bewegen

![Nach rechts](04_maze/script_move_right_initial.png "Nach rechts")

---

## Nach links?

Selbst machen!

---

## Lösung

![Seitwärts](04_maze/script_move_side_with_turn.png "Seitwärts")

---

## Hoch und runter?

Ebenfalls selbst machen!

---

# Das Labyrinth

---

# Bühne

* Bühne auswählen
* Registrierkarte "Hintergründe"
* Hintergrund-Bild hineinziehen
  (mit [Inkscape](https://inkscape.org/) gezeichnet)

![Bühne](04_maze/backdrop_maze.png "Bühne")

---

# Scales geht durch Wände?

---

# Lösung

![Blockierende Wände](04_maze/script_move_blocked_by_wall.png "Blockierende Wände")

---

# Scales sitzt beim Start in der Wand?

---

# Lösung

Zufall!

![Zufällige Platzierung](04_maze/script_random_placement_initial.png "Zufällige Platzierung")

---

# Scales startet noch immer manchmal in der Wand?

---

# Lösung

Mehr Zufall!

![Zufällige Platzierung](04_maze/script_random_placement.png "Zufällige Platzierung")

---

# Animation

---

# Flügelschlag

* Scales erhält mehrere Kostüme
* Diese werden in einer Schleife wiederholt gewechselt

---

# Flügelschlag

![Drache](04_maze/dragon0.png "Drache")
![Drache](04_maze/dragon1.png "Drache")
![Drache](04_maze/dragon2.png "Drache")
![Drache](04_maze/dragon1.png "Drache")

---

# Kostümwechsel

![Scales Animation](04_maze/scales_animation.png "Scales Animation")

---

# Komplettlösung für den Drachen Scales

![Alle Skripte von Scales](04_maze/scales_all_scripts.png "Alle Skripte von Scales")

---

# Punkte und Schatz

---

# Variablen

---

# Neue Punkte-Variable anlegen

![Neue Variable](04_maze/new_variable.png "Neue Variable")

![Neue Variable](04_maze/new_variable_points_name.png "Neue Variable")

![Neue Variable](04_maze/new_variable_points.png "Neue Variable")

---

# Punkte beim Start zurücksetzen

In den Skriptbereich der Bühne

![Punkte zurücksetzen](04_maze/script_stage_points_reset.png "Punkte zurücksetzen")

---

# Neues Objekt hinzufügen

![Schatztruhe](04_maze/treasure0.png "Schatztruhe")

Quelle: <https://opengameart.org/content/treasure-chests>

## Konfigurieren wie abgebildet

![Schatz-Objekt kofigurieren](04_maze/configure_treasure.png "Schatz-Objekt konfigurieren")

---

# Aus 1 mach viele

* Klonen der Schatztruhe beim Start
* Das Original versteckt sich

![Schatztruhe klonen](04_maze/script_clone_treasures.png "Schatztruhe klonen")

---

# Klone sollen sich anders verhalten

* Zufällige Position (wie Scales)
* Waren versteckt, müssen sich zeigen
* Wenn Scales den Klon berührt, folgendes durchführen:
    * Punkte hinzuzählen
    * Sich selbst entfernen

![Geklonte Schatztruhen](04_maze/script_treasures_cloned.png "Geklonte Schatztruhen")

---

# Luxusvariante: mit Animation

![Schatztruhe](04_maze/treasure0.png "Schatztruhe")
![Schatztruhe](04_maze/treasure1.png "Schatztruhe")
![Schatztruhe](04_maze/treasure2.png "Schatztruhe")

## Schatztruhe öffnen

![Schatztruhe öffnen](04_maze/script_treasure_open_animation.png "Schatztruhe öffnen")

## Schatztruhe langsam ausblenden

![Schatztruhe ausblenden](04_maze/script_treasure_disappear_animation.png "Schatztruhe ausblenden")

---

## Komplettlösung für geklonte Schatztruhen

![Komplette Animation](04_maze/script_treasure_cloned_complete.png "Komplette Animation")

---

# Das war's

Lasst eurer Fantasie freien Lauf!
