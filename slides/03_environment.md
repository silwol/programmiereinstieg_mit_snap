## Snap starten

* Webseite: <https://snap.berkeley.edu/>
* Run Snap! now

![Snap! starten](03_environment/website-enter.png "Snap! starten")

---

## Sprache umstellen

![Einstellungen](03_environment/language.png "Einstellungen")

![Deutsch](03_environment/language-german.png "Deutsch")

---

## Arbeitsumgebung

![Übersicht](03_environment/overview.png "Übersicht")

---

## Das Koordinatensystem

![Koordinatensystem](03_environment/grid.png "Koordinatensystem")

Quelle: <https://en.scratch-wiki.info/wiki/Coordinate_System>
